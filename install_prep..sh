#!/bin/bash
# install required applications and additional tools
sudo dnf install wget tar java-11-openjdk.x86_64 https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm -y

# Disable the built-in PostgreSQL module:
sudo dnf -qy module disable postgresql

# Install PostgreSQL:
sudo dnf install -y postgresql11-server

# update packages / kernel
dnf upgrade -y

# initialize the database and enable automatic start:
sudo /usr/pgsql-11/bin/postgresql-11-setup initdb
sudo systemctl enable postgresql-11
sudo systemctl start postgresql-11

#change postgres pg_hba.conf file from ident to trust

#restart postgres to pick up change
sudo systemctl restart postgresql-11
#create jira database, user and grant permissions
sudo -u postgres psql -f dbcreate

#create dedicated user to run Jira as
sudo /usr/sbin/useradd --create-home --comment "Account for running Jira Software" --shell /bin/bash jira

#configure firewall and sellinux
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-port=8080/tcp
firewall-cmd --reload

#temporarily make sellinux permissive
setenforce 0

#attempt to modify the config file to make it permanent (THIS IS A GUESS)
sed -c -i 's/\(SELINUX=enforcing\).*/\SELINUX=permissive/' /etc/selinux/config

#make directories for application
mkdir /data
mkdir /data/jira-home
cd /data

#get jira tar.gz file and import zip into current dir 
wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-8.14.0.tar.gz
wget http://download.isos.tech/TIS/jira-7-7-0-TIS.zip

#extract jira into curent folder
tar -xvf atlassian-jira-software-8.14.0.tar.gz -C /data/

#make softlink "jira" for easier upgrades
ln -s atlassian-jira-software-8.14.0-standalone/ jira

#change ownership and permissions on jira application and home folders
chown -R jira /data/jira/
chmod -R u=rwx,go-rwx /data/jira
chown -R jira /data/jira-data

#add JAVA_HOME and JIRA_HOME values to /etc/environment
sed -i '$ i\JAVA_HOME=/usr' /etc/environment
sed -i '$ i\JIRA_HOME=/data/jira-data' /etc/environment

#add jira.home value to jira-application.properties file
sed -i '$ i\jira.home=/data/jira-data' /data/jira/atlassian-jira/WEB-INF/classes/jira-application.properties

# move import file to /data/jira-data/import
mv /data/jira-7-7-0-TIS.zip /data/jira-data/import

# su to Jira and start application
#su jira
#/data/jira/bin/start-jira.sh

#Configure Jira to run as a service (may be premature since it hasn't been started yet, but oh well)
touch /lib/systemd/system/jira.service
chmod 664 /lib/systemd/system/jira.service

sed -i '$ i\[Unit]' /lib/systemd/system/jira.service
sed -i '$ i\Description=Atlassian Jira' /lib/systemd/system/jira.service
sed -i '$ i\After=network.target' /lib/systemd/system/jira.service

#add blank line
sed -i '$i\\'/lib/systemd/system/jira.service

sed -i '$ i\[Service]' /lib/systemd/system/jira.service
sed -i '$ i\Type=forking' /lib/systemd/system/jira.service
sed -i '$ i\User=jira' /lib/systemd/system/jira.service
sed -i '$ i\PIDFile=/data/jira/work/catalina.pid' /lib/systemd/system/jira.service
sed -i '$ i\ExecStart=/data/jira/bin/start-jira.sh' /lib/systemd/system/jira.service
sed -i '$ i\ExecStop=/data/jira/bin/stop-jira.sh' /lib/systemd/system/jira.service

#add blank line
sed -i '$i\\'/lib/systemd/system/jira.service

sed -i '$ i\[Install]' /lib/systemd/system/jira.service
sed -i '$ i\WantedBy=multi-user.target' /lib/systemd/system/jira.service

systemctl daemon-reload
systemctl enable jira.service
systemctl start jira.service
systemctl status jira.service

# tail logs to see what's happening
tail -f /data/jira/logs/catalina.out